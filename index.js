var request = require('request');

var pledgeUrl = 'https://kickstarter.getpebble.com/s/...';
var boxcarKey = '...';

console.log('[%s] Checking...', (new Date()).toJSON());
request({
    uri: pledgeUrl,
    jar: true
}, function(error, response, body) {
    if(error) {
        console.log(error);
        process.exit();
    }

    var re = new RegExp("data\-react\-props=\"(.*)\"", "g");
    var res = body.match(re);
    var data = res[0].substring(18, res[0].length - 1).replace(/&quot;/g, '"');
    data = JSON.parse(data);

    if(data.order.time_tracking_number != null) {

        console.log('Yes!');
        console.log(data);
        request.post({
            url: 'https://new.boxcar.io/api/notifications',
            form:{
                'user_credentials':boxcarKey,
                'notification[title]': 'Pebble shipped',
                'notification[long_message]': 'Tracking code: ' + data.order.time_tracking_number,
                'notification[sound]':'success',
                'notification[source_name]':'Pebble Shipment Tracker',
                'notification[icon_url]':'http://new.boxcar.io/images/rss_icons/boxcar-64.png',
                'notification[open_url]':'https://kickstarter.getpebble.com/s/lfwfFZppnKX7OlPEZGsU1Q',
            }
            }, function(err, result) {
                //console.log(err, result);
            }
        );
    } else {
        console.log('Sorry, no pebble for you (yet)');
    }
});
